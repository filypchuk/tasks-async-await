﻿using Common.DTO.Task;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace WebAPI.IntegrationTests
{
    public class TasksControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient client;
        public TasksControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            client = factory.CreateClient();
        }
        public void Dispose()
        {
        }
        [Fact]
        public async Task DeleteTask_ShouldWork()
        {
            int id = 1;
            var response = await client.DeleteAsync($"api/tasks/{id}");

            var getAllTasksResponse = await client.GetAsync("api/tasks");
            var stringTasksResponse = await getAllTasksResponse.Content.ReadAsStringAsync();
            var listUsers = JsonConvert.DeserializeObject<List<TaskDto>>(stringTasksResponse);

            Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
            Assert.True(listUsers.All(u => u.Id != id));
        }
        [Theory]
        [InlineData(-1)]
        [InlineData(0)]
        [InlineData(9999)]
        public async Task DeleteTask_Should_NotFound(int id)
        {
            var response = await client.DeleteAsync($"api/tasks/{id}");

            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}
