﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.MappingProfiles;
using BusinessLogicLayer.Services;
using Common.DTO.Team;
using DataAccessLayer.Context;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories;
using DataAccessLayer.Repositories.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BusinessLogicLayer.UnitTests
{
    public class TeamServiceTests : IDisposable
    {
        readonly ITeamService _service;
        readonly IMapper _mapper;
        readonly IRepository<Team> _repository;
        readonly InMemoryDBContext _inMemoryDB;
        readonly ProjectDbContext _context;
        public TeamServiceTests()
        {
            _mapper = MapperConfigurations();
            _inMemoryDB = new InMemoryDBContext();
            _context = _inMemoryDB.GetEmptyDbContext();
            _repository = new TeamRepository(_context);
            _service = new TeamService(_repository, _mapper);
        }
        [Fact]
        public async Task TeamAndUsersOlderTenYears_Should_AddUserToTeam1()
        {
            using (var context = _inMemoryDB.GetDbContext())
            {
                var user = new User { FirstName = Guid.NewGuid().ToString(), LastName = "LastName", Birthday = new DateTime(2000, 1, 1), TeamId = 1 };
                context.Users.Add(user);
                var actual = await _service.TeamAndUsersOlderTenYears();

                Assert.NotNull(actual.FirstOrDefault(t => t.Id == 1).Users.SingleOrDefault(u => u.FirstName == user.FirstName));
            }
        }

        public void Dispose()
        {
        }
        public IMapper MapperConfigurations()
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new ProjectProfile());
                mc.AddProfile(new TaskProfile());
                mc.AddProfile(new TeamProfile());
                mc.AddProfile(new UserProfile());
            });
            return mappingConfig.CreateMapper();
        }
    }
}
