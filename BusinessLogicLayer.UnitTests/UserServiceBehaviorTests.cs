﻿using AutoMapper;
using BusinessLogicLayer.Interfaces;
using BusinessLogicLayer.Services;
using Common.DTO.User;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using FakeItEasy;
using Xunit;

namespace BusinessLogicLayer.UnitTests
{
    public class UserServiceBehaviorTests
    {
        readonly IUserService _service;
        readonly IMapper _mapper;
        readonly IRepository<User> _repository;
        public UserServiceBehaviorTests()
        {
            _mapper = A.Fake<IMapper>();
            _repository = A.Fake<IRepository<User>>();
            _service = new UserService(_repository, _mapper);
        }
        [Fact]
        public void CreateUser_MustCallOnce()
        {
            _service.Create(new UserCreateDto());
            A.CallTo(() => _repository.CreateAsync(A<User>.Ignored)).MustHaveHappenedOnceExactly();
            A.CallTo(() => _repository.SaveAsync()).MustHaveHappenedOnceExactly();
        }
    }
}
