﻿using Common.DTO.Project;
using Common.DTO.Task;
using Common.DTO.User;

namespace Common.DTO.DtoForSevenLinqMethods
{
    public sealed class UsersProjectTaskDto
    {
        public UserDto User { get; set; }
        public ProjectDto LastProject { get; set; }
        public int CountTasksByLastProject { get; set; }
        public int CountStartCancelTasks { get; set; }
        public TaskDto TheLongestByTimeTask { get; set; }
    }
}
