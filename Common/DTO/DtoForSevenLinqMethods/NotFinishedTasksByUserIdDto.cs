﻿using Common.DTO.Project;
using Common.DTO.Task;
using System.Collections.Generic;

namespace Common.DTO.DtoForSevenLinqMethods
{
    public class NotFinishedTasksByUserIdDto
    {
        public ProjectDto Project { get; set; }
        public List<TaskDto> Tasks { get; set; }
    }
}
