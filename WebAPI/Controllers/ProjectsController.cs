﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLogicLayer.Interfaces;
using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Project;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public async Task<IEnumerable<ProjectDto>> GetAll()
        {
            return await _projectService.GetAll();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDto>> Get(int id)
        {

            var dto =await _projectService.GetById(id);
            if(dto != null)
                return Ok(dto);
            return NotFound();
        }
     
        [HttpPost]
        public async Task<ActionResult<ProjectDto>> Post([FromBody] ProjectCreateDto createDto)
        {
            var dto = await _projectService.Create(createDto);
            return Created($"{dto.Id}", dto);
        }

        [HttpPut]
        public async Task<ActionResult<ProjectDto>> Put([FromBody] ProjectDto updateDto)
        {
            await _projectService.Update(updateDto);
            return Ok(await _projectService.GetById(updateDto.Id));
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var dto = await _projectService.GetById(id);
            if (dto == null)                
                return NotFound();
            await _projectService.Delete(id);
            return NoContent();
        }

        [HttpGet("tasks-in-project/{id}")]
        public async Task<IEnumerable<ProjectAndCountTasksDto>> TasksInProjectByUser(int id)
        {
            return await _projectService.TasksInProjectByUser(id);
        }
        [HttpGet("all-projects-with")]
        public async Task<IEnumerable<ProjectAndTwoTasksDto>> AllProjectsWithTheLongestTaskAndTheShortest()
        {
            return await _projectService.AllProjectsWithTheLongestTaskAndTheShortest();
        }
        [HttpGet("all-notfinished-tasks-by-user/{id}")]
        public async Task<IEnumerable<NotFinishedTasksByUserIdDto>> AllNotFinishedTasksForEveryProject(int id)
        {
            return await _projectService.AllNotFinishedTasksForEveryProject(id);
        }
    }
}
