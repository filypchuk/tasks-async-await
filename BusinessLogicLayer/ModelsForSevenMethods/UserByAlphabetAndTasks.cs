﻿using DataAccessLayer.Entities;
using System.Collections.Generic;

namespace BusinessLogicLayer.ModelsForSevenMethods
{
    public sealed class UserByAlphabetAndTasks
    {
        public User User { get; set; }
        public List<TaskEntity> Tasks { get; set; }
    }
}
