﻿using DataAccessLayer.Entities;
using System.Collections.Generic;

namespace BusinessLogicLayer.ModelsForSevenMethods
{
    public class NotFinishedTasksByUserId
    {
        public Project Project { get; set; }
        public List<TaskEntity> Tasks { get; set; }
    }
}
