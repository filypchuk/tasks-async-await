﻿using DataAccessLayer.Context;
using DataAccessLayer.Entities;
using DataAccessLayer.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly ProjectDbContext _context;

        public ProjectRepository(ProjectDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Project>> GetAllAsync()
        {
            return await Task.Run(()=> 
                _context.Projects
                    .Include(p => p.Tasks)
                        .ThenInclude(t=>t.User)
                    .Include(p => p.Team)
                        .ThenInclude(t => t.Users)
                    .Include(p => p.User));
        }

        public async Task<Project> GetByIdAsync(int id)
        {
            return await _context.Projects.FindAsync(id);
        }

        public async Task<Project> CreateAsync(Project entity)
        {
            await _context.Projects.AddAsync(entity);
            return entity;
        }

        public async Task UpdateAsync(Project entity)
        {
            await Task.Run(() => _context.Projects.Update(entity));
        }

        public async Task DeleteAsync(int id)
        {
            Project entity = await _context.Projects.FindAsync(id);
            if (entity != null)
                await Task.Run(() => _context.Projects.Remove(entity));
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
