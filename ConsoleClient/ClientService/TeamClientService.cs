﻿using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Team;
using ConsoleClient.HttpRequests;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ConsoleClient.ClientService
{
    public class TeamClientService
    {
        private readonly HttpRequest _request;
        public TeamClientService()
        {
            _request = new HttpRequest();
        }
        public async Task<List<TeamDto>> GetAll()
        {
            return await _request.GetAll<TeamDto>("teams");
        }
        public async Task<TeamDto> GetById(int id)
        {
            return await _request.GetById<TeamDto>("teams", id);
        }
        public async Task<TeamDto> Create(TeamCreateDto createDto)
        {
            var json = JsonConvert.SerializeObject(createDto);
            return await _request.Create<TeamDto>("teams", json);
        }
        public async Task<TeamDto> Update(TeamDto updateDto)
        {
            var json = JsonConvert.SerializeObject(updateDto);
            return await _request.Update<TeamDto>("teams", json);
        }
        public async Task Delete(int id)
        {
            await _request.Delete("teams", id);
        }
        public async Task<List<TeamAndUsersDto>> TeamAndUsers()
        {
            return await _request.GetAll<TeamAndUsersDto>("teams/team-and-users");
        }
    }
}
