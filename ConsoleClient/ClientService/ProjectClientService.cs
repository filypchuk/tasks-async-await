﻿using Common.DTO.DtoForSevenLinqMethods;
using Common.DTO.Project;
using ConsoleClient.HttpRequests;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ConsoleClient.ClientService
{
    public class ProjectClientService
    {
        private readonly HttpRequest _request;
        public ProjectClientService()
        {
            _request = new HttpRequest();
        }
        public async Task<List<ProjectDto>> GetAll()
        {
           return await _request.GetAll<ProjectDto>("projects");
        }
        public async Task<ProjectDto> GetById(int id)
        {
           return await _request.GetById<ProjectDto>("projects", id);
        }
        public async Task<ProjectDto> Create(ProjectCreateDto createDto)
        {
            var json = JsonConvert.SerializeObject(createDto);
            return await _request.Create<ProjectDto>("projects", json);
        }
        public async Task<ProjectDto> Update(ProjectDto updateDto)
        {
            var json = JsonConvert.SerializeObject(updateDto);
            return await _request.Update<ProjectDto>("projects", json);
        }
        public async Task Delete(int id)
        {
            await _request.Delete("projects", id);
        }
        public async Task<Dictionary<ProjectDto, int>> CountTasksInProjectByUser(int id)
        {
           var p = await _request.GetById<IEnumerable<ProjectAndCountTasksDto>>("projects/tasks-in-project", id);
           return p.ToDictionary(p => p.Key, p => p.Value);
        }
        public async Task<List<ProjectAndTwoTasksDto>> AllProjectsWithTheLongestTaskAndTheShortest()
        {
            return await _request.GetAll<ProjectAndTwoTasksDto>("projects/all-projects-with");
        }
    }
}
