﻿using ConsoleClient.Helpers;
using ConsoleClient.View;
using System;
using System.Threading.Tasks;

namespace ConsoleClient.ConsoleMenu
{
    public class ProjectMenu
    {
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        private readonly ProjectView projectView;
        public ProjectMenu()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
            projectView = new ProjectView();
        }
        private void DisplayProjectMenu()
        {
            Console.Clear();
            print("Click button 1-7 to select\n", Color.Red);
            print("1---Get all Projects \n", Color.Green);
            print("2---Get Project by id \n", Color.Green);
            print("3---Create Project \n", Color.Blue);
            print("4---Update Project \n", Color.Yellow);
            print("5---Delete Project \n", Color.Red);
            print("6---Get the number of tasks in a project of a particular user (userId)\n", Color.Green);
            print("7---Project, the longest task, the shortest task, the total number of users in the project team\n", Color.Green);
            print("\n   Click Esc to back menu", Color.Red);
        }
        public async Task StartProjectMenu()
        {
            while (true)
            {
                DisplayProjectMenu();
                var keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.D1:
                        await projectView.AllProjects();
                        break;
                    case ConsoleKey.D2:
                        await projectView.ProjectById();
                        break;
                    case ConsoleKey.D3:
                        await projectView.CreateProject();
                        break;
                    case ConsoleKey.D4:
                        await projectView.UpdateProject();
                        break;
                    case ConsoleKey.D5:
                        await projectView.Delete();
                        break;
                    case ConsoleKey.D6:
                        await projectView.CountTasksInProjectByUser();
                        break;
                    case ConsoleKey.D7:
                        await projectView.AllProjectsWithTheLongestTaskAndTheShortest();
                        break;
                    case ConsoleKey.Escape:
                        return;
                    default:
                        break;
                }
            }
        }
    }
}
