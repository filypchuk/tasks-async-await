﻿using ConsoleClient.Helpers;
using ConsoleClient.View;
using System;
using System.Threading.Tasks;

namespace ConsoleClient.ConsoleMenu
{
    public class TaskMenu
    {
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        private readonly TaskView view;
        public TaskMenu()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
            view = new TaskView();
        }
        private void DisplayTaskMenu()
        {
            Console.Clear();
            print("Click button 1-7 to select\n", Color.Red);
            print("1---Get all Task \n", Color.Green);
            print("2---Get Task by id \n", Color.Green);
            print("3---Create Task \n", Color.Blue);
            print("4---Update Task \n", Color.Yellow);
            print("5---Delete Task \n", Color.Red);
            print("6---Get a list of tasks designed for a specific user (by id)\n", Color.Green);
            print("7---Get a list (id, name) from the collection of tasks that are finished\n", Color.Green);
            print("\n   Click Esc to back menu", Color.Red);
        }
        public async Task StartTaskMenu()
        {
            
            while (true)
            {
                DisplayTaskMenu();
                var keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.D1:
                        await view.AllTasks();
                        break;
                    case ConsoleKey.D2:
                        await view.TaskById();
                        break;
                    case ConsoleKey.D3:
                        await view.CreateTask();
                        break;
                    case ConsoleKey.D4:
                        await view.UpdateTask();
                        break;
                    case ConsoleKey.D5:
                        await view.Delete();
                        break;
                    case ConsoleKey.D6:
                        await view.TasksByUser();
                        break;
                    case ConsoleKey.D7:
                        await view.TasksFinishedByUser();
                        break;
                    case ConsoleKey.Escape:
                        return;
                    default:
                        break;
                }
            }
        }
    }
}
