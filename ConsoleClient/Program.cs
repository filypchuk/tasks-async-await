﻿using ConsoleClient.ConsoleMenu;
using System;
using System.Threading.Tasks;

namespace ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Loading...");
            MainMenu menu = new MainMenu();
            try
            {
                Task task = Task.Run(() => menu.Start());
                Task.WaitAll(task);
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
            }

        }
    }
}
