﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ConsoleClient.Helpers
{
    public class CheckOutput
    {
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        public CheckOutput()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
        }

        public bool NoContent(Object ob)
        {
            if (ob == null)
            {
                print("No Content", Color.Red);
                return true;
            }
            else
                return false;
        }
        public bool EmptyDto(int? dtoId)
        {
            if (dtoId == 0 || dtoId == null)
            {
                print("DTO has no content", Color.Red);
                return true;
            }
            else
                return false;
        }
        public bool EmptyList<T>(IEnumerable<T> ob)
        {
            if (ob.Count() == 0 || ob == null)
            {
                print("Empty List", Color.Red);
                return true;
            }
            else
                return false;
        }
    }
}
