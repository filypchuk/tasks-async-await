﻿using Common.DTO.Project;
using ConsoleClient.ClientService;
using ConsoleClient.Helpers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ConsoleClient.View
{
    public class ProjectView
    {
        private readonly ProjectClientService _service;
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        private readonly WaitingForEnter printWaitingForEnter;
        private readonly CheckInput checkInput;
        private readonly CheckOutput checkOutput;
        public ProjectView()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
            printWaitingForEnter = printToConsole.WaitEnter;
            checkInput = new CheckInput();
            checkOutput = new CheckOutput();
            _service = new ProjectClientService();
        }
        public async Task AllProjects()
        {
            Console.Clear();
            print("Get all projects", Color.Yellow);
            var listDto = await _service.GetAll();
            if (!checkOutput.EmptyList(listDto))
            {
                foreach(var dto in listDto)
                {
                    print(dto.ToString(), Color.Yellow);
                }             
            }
            printWaitingForEnter();
        }
        public async Task ProjectById()
        {
            Console.Clear();
            print("Get project by id", Color.Yellow);
            print("Enter project id", Color.Green);
            int id = checkInput.CheckingInt();
            var dto = await _service.GetById(id);
            if (!checkOutput.EmptyDto(dto?.Id))
            { 
                print(dto.ToString(), Color.Yellow); 
            }
            printWaitingForEnter();
        }
        public async Task CreateProject()
        {
            Console.Clear();
            print("Create project", Color.Yellow);
            ProjectCreateDto createDto = new ProjectCreateDto();
            print("Enter name", Color.Green);
            createDto.Name = checkInput.CheckingString();
            print("Enter Description", Color.Green);
            createDto.Description = checkInput.CheckingString();
            print("Enter Deadline", Color.Green);
            createDto.Deadline = checkInput.CheckingDateTime();
            print("Enter AuthorId", Color.Green);
            createDto.AuthorId = checkInput.CheckingInt();
            print("Enter TeamId", Color.Green);
            createDto.TeamId = checkInput.CheckingInt();

            var dto = await _service.Create(createDto);
            if (!checkOutput.EmptyDto(dto?.Id))
            {
                print(dto.ToString(), Color.Yellow);
            }
            printWaitingForEnter();
        }
        public async Task UpdateProject()
        {
            Console.Clear();
            print("Update project", Color.Yellow);
            print("Enter project id", Color.Green);
            int id = checkInput.CheckingInt();
            var dto = await _service.GetById(id);
            if (!checkOutput.EmptyDto(dto?.Id))
            {
                print(dto.ToString(), Color.Yellow);
                ProjectDto updateDto = new ProjectDto();
                updateDto.Id = dto.Id;
                print("Enter new name", Color.Green);
                updateDto.Name = checkInput.CheckingString();
                print("Enter new Description", Color.Green);
                updateDto.Description = checkInput.CheckingString();
                print("Enter new Deadline", Color.Green);
                updateDto.Deadline = checkInput.CheckingDateTime();
                print("Enter new AuthorId", Color.Green);
                updateDto.AuthorId = checkInput.CheckingInt();
                print("Enter new TeamId", Color.Green);
                updateDto.TeamId = checkInput.CheckingInt();
                updateDto.CreatedAt = dto.CreatedAt;

                var responseDto = await _service.Update(updateDto);
                print("Updated dto", Color.Green);
                if (!checkOutput.EmptyDto(responseDto?.Id))
                {
                    print(responseDto.ToString(), Color.Yellow);
                }
            }          
            printWaitingForEnter();
        }
        public async Task Delete()
        {
            Console.Clear();
            print("Delete project", Color.Yellow);
            print("Enter project id", Color.Green);
            int id = checkInput.CheckingInt();
            await _service.Delete(id);
            printWaitingForEnter();
        }
        public async Task CountTasksInProjectByUser()
        {
            Console.Clear();
            print("First Method", Color.Yellow);
            print("Get the number of tasks in the project of a particular user (by id)\n" +
                "(dictionary, where the key will be the project, and value the number of tasks).", Color.None);
            print("Enter user id", Color.Green);
            int userId = checkInput.CheckingInt();
            var dic = await _service.CountTasksInProjectByUser(userId);
            if (!checkOutput.EmptyList(dic))
            {
                foreach (KeyValuePair<ProjectDto, int> keyValue in dic)
                {
                    print("Project", Color.Yellow);
                    print(keyValue.Key.ToString(), Color.Green);
                    print("Total tasks in project " + keyValue.Value, Color.Yellow);
                    print("-------------------------------", Color.Blue);
                }
            }
            else print("User not found or user doesn't have any project or something else", Color.Red);
            printWaitingForEnter();
        }
        public async Task AllProjectsWithTheLongestTaskAndTheShortest()
        {
            Console.Clear();
            print("Projects", Color.Yellow);
            var list = await _service.AllProjectsWithTheLongestTaskAndTheShortest();
            foreach (var i in list)
            {
                print("Project", Color.Yellow);
                print(i.Project.ToString(), Color.Green);
                print("The longest task of the project (by description)", Color.Yellow);
                if (!checkOutput.NoContent(i.TheLongestTask))
                {
                    print(i.TheLongestTask.ToString(), Color.Green);
                }
                print("The shortest task of the project (by name)", Color.Yellow);
                if (!checkOutput.NoContent(i.TheShortestTask))
                {
                    print(i.TheShortestTask.ToString(), Color.Green);
                }
                print("The total number of users in the project team, \n " +
                    "where either the project description > 20 characters, or the count of tasks < 3", Color.Yellow);
                print(i.CountUsersInTeam.ToString(), Color.Green);
                print("-------------------------------", Color.Blue);
            }
            printWaitingForEnter();
        }
    }
}
