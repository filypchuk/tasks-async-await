﻿using Common.DTO.Task;
using ConsoleClient.ClientService;
using ConsoleClient.Helpers;
using System;
using System.Threading.Tasks;

namespace ConsoleClient.View
{
    public class TaskView
    {
        private readonly TaskClientService _service;
        private readonly PrintToConsole printToConsole;
        private readonly Printing<string, Color> print;
        private readonly WaitingForEnter printWaitingForEnter;
        private readonly CheckInput checkInput;
        private readonly CheckOutput checkOutput;
        public TaskView()
        {
            printToConsole = new PrintToConsole();
            print = printToConsole.PrintColor;
            printWaitingForEnter = printToConsole.WaitEnter;
            checkInput = new CheckInput();
            checkOutput = new CheckOutput();
            _service = new TaskClientService();
        }
        public async Task AllTasks()
        {
            Console.Clear();
            print("Get all tasks", Color.Yellow);
            var listDto = await _service.GetAll();
            if (!checkOutput.EmptyList(listDto))
            {
                foreach (var dto in listDto)
                {
                    print(dto.ToString(), Color.Yellow);
                }
            }
            printWaitingForEnter();
        }
        public async Task TaskById()
        {
            Console.Clear();
            print("Get task by id", Color.Yellow);
            print("Enter project id", Color.Green);
            int id = checkInput.CheckingInt();
            var dto = await _service.GetById(id);
            if (!checkOutput.EmptyDto(dto?.Id))
            {
                print(dto.ToString(), Color.Yellow);
            }
            printWaitingForEnter();
        }
        public async Task CreateTask()
        {
            Console.Clear();
            print("Create task", Color.Yellow);
            TaskCreateDto createDto = new TaskCreateDto();
            print("Enter name", Color.Green);
            createDto.Name = checkInput.CheckingString();
            print("Enter Description", Color.Green);
            createDto.Description = checkInput.CheckingString();
            print("Enter FinishedAt", Color.Green);
            createDto.FinishedAt = checkInput.CheckingDateTime();
            print("Enter ProjectId", Color.Green);
            createDto.ProjectId = checkInput.CheckingInt();
            print("Enter PerformerId", Color.Green);
            createDto.PerformerId = checkInput.CheckingInt();

            var dto = await _service.Create(createDto);
            if (!checkOutput.EmptyDto(dto?.Id))
            {
                print(dto.ToString(), Color.Yellow);
            }
            printWaitingForEnter();
        }
        public async Task UpdateTask()
        {
            Console.Clear();
            print("Update task", Color.Yellow);
            print("Enter task id", Color.Green);
            int id = checkInput.CheckingInt();
            var dto = await _service.GetById(id);
            if (!checkOutput.EmptyDto(dto?.Id))
            {
                print(dto.ToString(), Color.Yellow);
                TaskDto updateDto = new TaskDto();
                updateDto.Id = dto.Id;
                print("Enter new name", Color.Green);
                updateDto.Name = checkInput.CheckingString();
                print("Enter new Description", Color.Green);
                updateDto.Description = checkInput.CheckingString();
                print("Enter new FinishedAt", Color.Green);
                updateDto.FinishedAt = checkInput.CheckingDateTime();
                print("Enter new ProjectId", Color.Green);
                updateDto.ProjectId = checkInput.CheckingInt();
                print("Enter new PerformerId", Color.Green);
                updateDto.PerformerId = checkInput.CheckingInt();
                updateDto.CreatedAt = dto.CreatedAt;
                updateDto.State = dto.State;

                var responseDto = _service.Update(updateDto);
                print("Updated dto", Color.Green);
                if (!checkOutput.EmptyDto(responseDto?.Id))
                {
                    print(responseDto.ToString(), Color.Yellow);
                }
            }
            printWaitingForEnter();
        }
        public async Task Delete()
        {
            Console.Clear();
            print("Delete task", Color.Yellow);
            print("Enter task id", Color.Green);
            int id = checkInput.CheckingInt();
            await _service.Delete(id);
            printWaitingForEnter();
        }
        public async Task TasksByUser()
        {
            Console.Clear();
            print("Get a list of tasks designed for a specific user (by id),\n" +
                "where name task < 45 characters (collection of tasks).", Color.None);
            print("Enter user id", Color.Green);
            int userId = checkInput.CheckingInt();
            var tasks = await _service.TasksByUser(userId);
            if (!checkOutput.EmptyList(tasks))
            {
                foreach (var task in tasks)
                {
                    print("Task", Color.None);
                    print(task.ToString(), Color.Green);
                    print("-------------------------------", Color.Blue);
                }
            }
            else print("User not found or user doesn't have any task or something else", Color.Red);
            printWaitingForEnter();
        }
        public async Task TasksFinishedByUser()
        {
            Console.Clear();
            print("Get a list (id, name) from the collection of tasks that are finished \n" +
                "in the current (2020) year for a specific user (by id).", Color.None);
            print("Enter user id", Color.Green);
            int userId = checkInput.CheckingInt();
            var list = await _service.TasksFinishedByUser(userId);
            if (!checkOutput.EmptyList(list))
            {
                foreach (var dto in list)
                {
                    print("Task", Color.None);
                    print($"Id -- {dto.Id} \t Name -- {dto.Name}", Color.Green);
                    print("-------------------------------", Color.Blue);
                }
            }
            else print("User not found or user didn't finish any task in 2020 or something else", Color.Red);
            printWaitingForEnter();
        }
    }
}
